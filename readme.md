#Visualisaton
My visualisation show some statistics on a selected user:

* How many commits they have made to their repositories
* How many commits, on average, they make per hour
* What time period have they worked on a repository

this information can be used to check if a developer has been commiting their changes as often as they should
Note: since the data takes into account the dates of the first ever commit and the last ever commit, any long hiatuses can cause inaccuracies

[An example page with results can be found here](https://bitbucket.org/sloder75/sweng-github-visualisation/src/0d1c8fbf2a3d8e9cddc6eb67fab8d6bab8ac8b8e/example.html?at=master&fileviewer=file-view-default)

#Requirements
You will need to use node.js to run the program.
The d3node dependency should install itself but if it doesn't happen, you can get it [here.](https://github.com/d3-node/d3-node)
You can also simply try "npm install d3-node".

To get the webpage, connect to localhost on port 8080.

To increase the amount of queries you can perform in an hour, you should authenticate yourself with a github token.
[You can find help on creating a token here.](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/)
You do not need to give it any permissions, it is only used to authenticate yourself so that more queries can be done in a hour.
Without authentication you can probably still do a querry or two but after that the limit will be used up.
You should pass the token to the application as its first argument, like this: "npm index.js _token_".

Lastly, to avoid weird behaviour with sockets, i had to force the api calls to execute sequentialy.
This means that if the user you're checking out has a lot of repositories, it might take a moment to produce the results.

#Github access (part due week 9)
This part was finished at the commit [776d2c6](https://bitbucket.org/sloder75/sweng-github-visualisation/commits/776d2c6666389d2bd525427f210a6b0d25d963e2) where the program would ask for the repos of the octokit organisation and display them on localhost:8080.