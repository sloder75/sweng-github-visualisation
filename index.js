const http = require('http');
const https = require('https');
const D3Node = require('d3-node');
const d3 = require('d3');
const qs = require('querystring');
const fs = require('fs');

const hostname = '127.0.0.1';
const port = 8080;

const args = process.argv.slice(2);
var fileContent = '';

var apiLimit;
var apiCallsLeft;

const chartWidth = 900;
const chartHeight = 400;
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//see if the user provided an authentication token for the api
function getUserOptions(user){
	var options;
	if(args.length === 1){
		options = {
			host: 'api.github.com',
			path: `/users/${user}/repos`,
			headers: {'user-agent':'myTestApp', 'Authorization':`token ${args[0]}`}
		};
	}else{
		options = {
			host: 'api.github.com',
			path: `/users/${user}/repos`,
			headers: {'user-agent':'myTestApp'}
		};
	}
	return options;
}

function getRepoOptions(user,repo){
	var options;
	if(args.length === 1){
		options = {
			host: 'api.github.com',
			path: `/repos/${user}/${repo}/commits`,
			headers: {'user-agent':'myTestApp', 'Authorization':`token ${args[0]}`}
		};
	}else{
		options = {
			host: 'api.github.com',
			path: `/repos/${user}/${repo}/commits`,
			headers: {'user-agent':'myTestApp'}
		};
	}
	return options;
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//make a timechart out of json data
function makeTimeChart(data){
	var chartHeight = 900;
	margin = {top: 20, right: 30, bottom: 50, left: 30}
	width = chartWidth - margin.left - margin.right;
	height = chartHeight - margin.top - margin.bottom;

	var d3n = new D3Node();
	var chart = d3n.createSVG(chartWidth,chartHeight).attr('class','chart');

	var y = d3.scaleBand()
		.domain(data.map(d => d.tag))
		.range([height - margin.bottom, margin.top])
		.padding(0.1);
		
	var x = d3.scaleTime()
		.domain([d3.min(data, d => new Date(d.startDate)), d3.max(data, d => new Date(d.endDate))]).nice()
		.range([margin.left, width - margin.right]);
		
	var xAxis = g => g
		.attr("transform", `translate(0,${height - margin.bottom})`)
		.call(d3.axisBottom(x).tickSizeOuter(0))
		.selectAll("text")  
    .style("text-anchor", "end")
    .attr("dx", "-.8em")
    .attr("dy", ".15em")
    .attr("transform", "rotate(-45)");

	var yAxis = g => g
		.attr("transform", `translate(${width - margin.right},0)`)
		.call(d3.axisRight(y))
		.style("text-anchor","start")
		.call(g => g.select(".domain").remove());

	var dataspace = chart.append("g").attr("class","barcontainer")
		.selectAll("rect")
		.data(data)
		.enter();
	
	dataspace.append("rect")
		.attr("class","background")
		.attr("x", margin.left)
		.attr("y", function(d) { return y(d.tag); })
		.attr("height", y.bandwidth())
		.attr("width", width-margin.right-margin.left);
		
	dataspace.append("rect")
		.attr("class", "bar")
		.attr("x", function(d) { return x(new Date(d.startDate)); })
		.attr("y", function(d) { return y(d.tag); })
		.attr("height", y.bandwidth())
		.attr("width", function(d) { return x(new Date(d.endDate)) - x(new Date(d.startDate));});

	chart.append("g")
		.attr("class", "x axis")
		.call(xAxis);

	chart.append("g")
		.attr("class", "y axis")
		.call(yAxis);

	return d3n.svgString();
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//make a barchart out of json data
function makeBarchart(data){
	margin = {top: 20, right: 30, bottom: 50, left: 30}
	width = chartWidth - margin.left - margin.right;
	height = chartHeight - margin.top - margin.bottom;

	var d3n = new D3Node();
	var chart = d3n.createSVG(chartWidth,chartHeight).attr('class','chart');

	var x = d3.scaleBand()
		.domain(data.map(d => d.tag))
		.range([margin.left, width - margin.right])
		.padding(0.1);

	var y = d3.scaleLinear()
		.domain([0, d3.max(data, d => d.value)]).nice()
		.range([height - margin.bottom, margin.top]);
		
	var xAxis = g => g
		.attr("transform", `translate(0,${height - margin.bottom})`)
		.call(d3.axisBottom(x).tickSizeOuter(0))
		.selectAll("text")  
    .style("text-anchor", "end")
    .attr("dx", "-.8em")
    .attr("dy", ".15em")
    .attr("transform", "rotate(-45)");

	var yAxis = g => g
		.attr("transform", `translate(${margin.left},0)`)
		.call(d3.axisLeft(y))
		.style("text-anchor","end")
		.call(g => g.select(".domain").remove());

	chart.append("g")
		.attr("class", "x axis")
		.call(xAxis);

	chart.append("g")
		.attr("class", "y axis")
		.call(yAxis);

	chart.append("g")
		.selectAll("rect")
		.data(data)
		.enter().append("rect")
		.attr("class", "bar")
		.attr("x", function(d) { return x(d.tag); })
		.attr("y", function(d) { return y(d.value); })
		.attr("height", function(d) { return y(0) - y(d.value); })
		.attr("width", x.bandwidth());

	return d3n.svgString();
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//load the html page template
loadFile = new Promise(resolve => {
	fs.readFile('./index.html', (err,text) => {
		fileContent = text.toString();
		resolve(text.toString());
	});
});

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//read the content of a http response
function readContent(request,callback){
	data = '';
	request.on('data',(chunk)=>{
		data +=chunk;
	});
	request.on('end',()=>{
		callback(data);
	});
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//make a request to the github api
function makeApiRequest(options){
	return new Promise((resolve,reject)=>{
		https.get(options, (res) => {
			console.log("Requesting the api for info.");
			console.log(res.statusCode);

			apiLimit = res.headers['x-ratelimit-limit'];
			apiCallsLeft = res.headers['x-ratelimit-remaining'];
	
			readContent(res,(response)=>{
				json = JSON.parse(response);
				json.statusCode = res.statusCode;
				resolve(json);
		
				console.log("Done with the api call.");
			});
		});
	});
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//get the statistics for a user
function getStatistics(user){
	return new Promise(async (resolve)=>{
		options = getUserOptions(user);
		
		 info = await makeApiRequest(options);
		 var answers = [];
		 for(var i = 0; i < info.length; i++){
			 options = getRepoOptions(user,info[i].name);
			 repos = await makeApiRequest(options);
			 if(repos.statusCode == 200)
			 	answers.push({
					 repo: info[i].name,
					 commits: repos.length,
					 endDate: repos[0].commit.author.date,
					 startDate: repos[repos.length-1].commit.author.date
			 	});
		 }				
		 answers.user = user;
		 resolve(answers);
	});
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//define the server behaviour
const server = http.createServer((req, res) => {
	console.log("Got a request.");

	getResponseContent = new Promise((resolve, reject)=>{
		console.log(req.method);
		//ignore favicon requests
		if(req.url.includes('favicon.ico')) reject("no favicon for you");
		else{
			if(req.method == 'GET'){
				//query the default api point
				getStatistics('sloder75').then((stat,reject)=>{
					resolve(stat);
				});
			}else{
				//query the api for a selected user
				readContent(req,(response)=>{
					data = qs.parse(response);
					getStatistics(data.target).then((stat,reject)=>{
						resolve(stat);
					});
				});
			}
		}
	});

	//return found data
	getResponseContent.then(stat=>{

		var totalCommits = [];
		for(var i = 0; i < stat.length; i++){
			totalCommits.push({
				tag: stat[i].repo,
				value: stat[i].commits
			});
		}

		var commitDensity = [];
		for(var i = 0; i < stat.length; i++){
			var endDate = new Date(stat[i].endDate);
			var startDate = new Date(stat[i].startDate);
			var diff =  Math.max(Math.round(( Date.parse(endDate) - Date.parse(startDate) ) / 3600000), 1); 

			value = stat[i].commits / diff;

			commitDensity.push({
				tag: stat[i].repo,
				value: value
			});
		}
		
		var timeline = [];
		for(var i = 0; i < stat.length; i++){
			timeline.push({
				tag: stat[i].repo,
				value: stat[i].commits,
				startDate: stat[i].startDate,
				endDate: stat[i].endDate
			});
		}

		var totalCommitsChart = makeBarchart(totalCommits);
		var commitDensityChart = makeBarchart(commitDensity);
		var timelineChart = makeTimeChart(timeline);

		//prepare html body
		text = fileContent;
		text = text.replace('LIMIT', apiLimit);
		text = text.replace('REMAINING', apiCallsLeft);
		text = text.replace('USER', stat.user);
		text = text.replace('REPOS', stat.length);
		text = text.replace('TOTAL', totalCommitsChart);
		text = text.replace('DENSITY', commitDensityChart);
		text = text.replace('TIMELINE', timelineChart);

		//setup response
		res.statusCode = 200;
		res.setHeader('Content-Type', 'text/html');
		res.write(text);
		res.end();
		console.log("Responded to the request.\n");

	}).catch(reject=>{
		res.statusCode = 403;
		res.setHeader('Content-Type', 'text/html');
		res.end();
		console.log(reject);
		console.log("Responded to the request.\n");
	});
});
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//start the server
loadFile.then(()=>{
		server.listen(port, hostname, () => {
  	console.log(`Server running at http://${hostname}:${port}/`);
	});
});
